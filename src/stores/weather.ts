import { defineStore } from "pinia";

interface State {
	today: Date;
};

export const useWeatherStore = defineStore('weather', {

	state: (): State => ({
		today: new Date()
	}),
	getters: {

	},
	actions: {

	}
});