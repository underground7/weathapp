# Weathapp

This template should help get you started developing with Vue 3 in Vite.

## Project Setup

The easiest way to run the application is to have docker-compose installed, then being in the main directory of the project, enter the command docker-compose up -d in the console. If you don't have docker-compose, feel free to visit [here](https://docs.docker.com/compose/install/)

You can alternatively run the application. Node environment and npm package manager are required.

If you already have the above packages, start the console and navigate to the root directory of the project. Then type the following commands one by one

```sh
npm install
npm run dev
```

Open any browser and in the URL bar type http://127.0.0.1:8080